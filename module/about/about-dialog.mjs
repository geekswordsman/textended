import { TExtended } from "../textended.mjs";

export class AboutDialog extends FormApplication {
    constructor(object, options) {
        super(object, options);
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            id: "TEXtendedAboutDialog",
            closeOnSubmit: false,
            height: "auto",
            width: 550,
            submitOnChange: true,
            template: 'modules/textended/module/about/about-dialog.hbs',
            title: game.i18n.localize("TEXTENDED.ABOUT.about"),
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    activateListeners(html) {
        super.activateListeners(html);
    }

    getData(options) {
        return {
            "version": TExtended.VERSION
        };
    }
}