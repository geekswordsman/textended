import { TExtended } from './textended.mjs';
import { Logger } from "./logger/logger.mjs";

/* Init Logger */
Logger.MODULE_ID = TExtended.MODULE_ID;

/*Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(TExtended.MODULE_ID);
});

Hooks.once('init', () => { TExtended.initialize(); })

Handlebars.registerHelper('selected', function (a, b) {
    if (a == b) {
        return 'selected';
    }

    return '';
});