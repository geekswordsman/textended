# TExtended
## v1.2.2 - 2022-09-25
 - Corrected bug where config menu was not functioning correctly (#3)
## v1.2.1 - 2022-08-09
 - Foundry VTT v10 Compatibility Update
 - Added simple about dialog
 - Added improved Logger class

## v1.2.0 - 2022-02-21
 - Merged Font Awesome Plugin, provided by @arcanist#4317!

## v1.1.0 - 2022-01-24
 - Merged Templates system, provided by @arcanist#4317!
 - Expanded the list of block and inline elements available for use!

## v1.0.1 - 2021-12-07
- Added in style preview to the Tiny MCE Menu (thanks to Arcanist for the code!)
- Updated the sample styles for narration and note blocks to use showcase simple use of CSS gradients.

## v1.0.0 - 2021-11-24
- First release of TExtended